#!/bin/bash
function printUsage(){
echo "`basename $0` is a script to locally install dependencies for crosscontam. You can choose to install all dependencies or a specific one.

usage :
$0 --tool all|B|K|R|BL --os ubuntu|debian|fedora|centos|redhat|macosx
"
}
function printAndUsageAndExit(){
    echo
    echo "!!! Fatal error !!!"
    echo $1
    echo
    echo "-----------------------------------------------------------------------------------------"
    printUsage
    exit 1
}

### installing various utilities for MacOSX users only
if [[ $OSTYPE == darwin* ]]; then
 # brew
 /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
 export PATH="$(brew --prefix coreutils)/libexec/gnubin:/usr/local/bin:$PATH"
 # gnu-like tools
 brew tap homebrew/dupes
 brew tap homebrew/versions
 brew install coreutils gnu-getopt gawk grep gnu-sed gcc48 make cmake
 brew link --force gnu-getopt
# sudo chown -R $USER /usr/local/lib 
# brew link --force gcc48
### installing various utilities for linux users only
elif [[ $OSTYPE == linux* ]]; then
 sudo apt-get install software-properties-common
 sudo add-apt-repository ppa:george-edison55/cmake-3.x
 sudo apt-get update
 sudo apt-get install cmake
fi


### getting the script arguments

ARGS=$(getopt -o t: --long tool:,os: -n "$0" -- "$@");

#Bad arguments
if [ $? -ne 0 ] || [ $# -eq 0 ];
then
    printUsage
    exit
fi
TOOLFLAG=0
OSFLAG=0
eval set -- "$ARGS";

while true; do
    case "$1" in
        --os)
            shift;
            if [ -n "$1" ]; then
                if [[ "$1" == "ubuntu" ]] || [[ "$1" == "debian" ]] || [[ "$1" == "fedora" ]] || [[ "$1" == "redhat" ]] || [[ "$1" == "centos" ]] || [[ "$1" == "macosx" ]]; then
                    OS=$1
                    OSFLAG=1
                else
                    printAndUsageAndExit "'$1' is an incorrect value for --os option (ubuntu|debian|fedora|centos|redhat|macosx are correct values)"
                fi
            else
                printAndUsageAndExit "You have to set a non-empty value for option --os"
            fi
            shift;
            ;;
        --tool)
            shift;
            if [ -n "$1" ]; then
                if [[ "$1" == "all" ]] || [[ "$1" == "B" ]] || [[ "$1" == "K" ]] || [[ "$1" == "R" ]] || [[ "$1" == "BL" ]]; then
                    TOOL=$1
                    TOOLFLAG=1
                else
                    printAndUsageAndExit "'$1' is an incorrect value for --tool option (all, B, K, R and BL are correct values)"
                fi
            else
                printAndUsageAndExit "You have to set a non-empty value for option --tool"
            fi
            shift;
            ;;
        --)
            break;
            ;;
    esac
done

if ((OSFLAG == 0)); then
    printAndUsageAndExit "You must set the --os option"
fi
if ((TOOLFLAG == 0)); then
    printAndUsageAndExit "You must set the --tool option"
fi

############################## DEPENDENCIES FOR TOOLS (SYSTEM DEPENDENT) ################################################

if [[ "$OS" == "debian" ]] || [[ "$OS" == "ubuntu" ]]; then
    # check sub dependencies :
    # TODO for hpg-aligner : scons zlib1g-dev libcurl4-gnutls-dev libxml2-dev libncurses5-dev libgsl0-dev check
    PACKAGELIST="g++
libstdc++-.*-dev
libpng12-dev
zlib1g-dev
zip
unzip
tar
gzip
scons
libcurl4-gnutls-dev
libxml2-dev
libncurses5-dev
check
gcc
cmake
autoconf
libbz2-dev
liblzma-dev"

    PACKAGES_TO_INSTALL=""
    for prog in $PACKAGELIST; do
        if [[ "`dpkg --get-selections | awk '{print $1}' | awk -F : '{print $1}' | grep "^$prog$" -c`" == "0" ]]; then
            PACKAGES_TO_INSTALL+="${prog/\.\*/*} "
        fi
    done
    if [[ "$PACKAGES_TO_INSTALL" != "" ]]; then
        echo "I see some missing packages in your system."
        echo "You must install those packages with APT : $PACKAGES_TO_INSTALL"
        echo "Would you like me to do it now ? (Y/n)"
        read rep
        if [[ "$rep" == "Y" ]] || [[ "$rep" == "y" ]] || [[ "$rep" == "" ]]; then
            echo sudo apt-get install $PACKAGES_TO_INSTALL
            sudo apt-get install $PACKAGES_TO_INSTALL
        fi
    fi

elif [[ "$OS" == "redhat" ]] || [[ "$OS" == "fedora" ]] || [[ "$OS" == "centos" ]]; then
    PACKAGELIST="zlib-devel
libpng-devel
unzip
scons
libcurl-devel
libxml2-devel
ncurses-devel
gsl-devel
check
gcc
gcc-c++
cmake
autoconf
bzip2-devel
lzma-devel
libstdc++-devel
libstdc++-static
check-devel"
    PACKAGES_TO_INSTALL=""
    for prog in $PACKAGELIST; do
        if [[ "`yum list installed 2> /dev/null | awk '{print $1}' | awk -F '.' '{print $1}' | grep "^$prog$" -c`" == "0" ]]; then
            PACKAGES_TO_INSTALL+="$prog "
        fi
    done
    if [[ "$PACKAGES_TO_INSTALL" != "" ]]; then
        echo "I see some missing packages in your system."
        echo "You must install those packages with YUM : $PACKAGES_TO_INSTALL"
        echo "Would you like me to do it now ? (Y/n)"
        read rep
        if [[ "$rep" == "Y" ]] || [[ "$rep" == "y" ]] || [[ "$rep" == "" ]]; then
            echo sudo yum install $PACKAGES_TO_INSTALL
            sudo yum install $PACKAGES_TO_INSTALL
        fi
    fi
elif [[ "$OS" == "macosx" ]]; then
    echo
fi

########################## TOOL INSTALLATION ####################################################"

cd bin/
echo

# bowtie
if [[ "$TOOL" == "all" ]] || [[ "$TOOL" == "B" ]]; then
    echo "Removing old install of Bowtie (within CroCo) if it exists"
    rm -rf bowtie-1.1.2
    echo "Installing Bowtie"
    if [[ "$OS" == "macosx" ]]; then
        unzip bowtie-1.1.2-macos-x86_64.zip > bowtie_install.log 2>&1
        ret=$?
    else
        unzip bowtie-1.1.2-linux-x86_64.zip > bowtie_install.log 2>&1
        ret=$?
    fi
    if ((ret==0)) && [ -f bowtie-1.1.2/bowtie ]; then
        bp=$(readlink -f bowtie-1.1.2/)
        echo OK !! Bowtie install in $bp OK. It will be found automatically by crosscontam.
    else
        echo "!! ERROR !!"
        echo Bowtie install error
    fi
    echo
fi


## bowtie2
#if [[ "$TOOL" == "all" ]] || [[ "$TOOL" == "B2" ]]; then
#    echo "Removing old install of Bowtie2 (within CroCo) if it exists"
#    rm -rf bowtie2-2.2.9
#    echo "Installing Bowtie2"
#    if [[ "$OS" == "macosx" ]]; then
#        unzip bowtie2-2.2.9-macos-x86_64.zip > bowtie2_install.log 2>&1
#        ret=$?
#    else
#        unzip bowtie2-2.2.9-linux-x86_64.zip > bowtie2_install.log 2>&1
#        ret=$?
#    fi
#    if ((ret==0)) && [ -f bowtie2-2.2.9/bowtie2 ]; then
#        bp=$(readlink -f bowtie2-2.2.9/)
#        echo OK !! Bowtie2 install in $bp OK. It will be found automatically by crosscontam.
#    else
#        echo "!! ERROR !!"
#        echo Bowtie2 install error
#        echo
#    fi
#    echo
#fi

### rapmap

if [[ "$TOOL" == "all" ]] || [[ "$TOOL" == "R" ]]; then
    echo "Removing old install of Rapmap (within CroCo) if it exists"
    rm -rf rapmap
    if [[ "$OS" == "macosx" ]]; then
        which cmake > /dev/null
        retcmake=$?
        which make > /dev/null
        retmake=$?
        which clang > /dev/null
        retclang=$?
        which g++ > /dev/null
        retgpp=$?
        # if make and cmake are found and one of clang/g++ is found
        if ((retcmake == 0)) && ((retmake == 0)) && ( ((retclang == 0)) || ((retgpp == 0)) ); then
            echo "make, cmake, clang/g++ were found on your system"
        else
            echo "You must install make, cmake and one of clang and g++ to compile RapMap"
            exit 0
        fi
    fi
    echo "Installing Rapmap. This may take more than 5 minutes."
    tar xf rapmap.tar.gz > rapmap_install.log 2>&1
    cd rapmap
    mkdir build
    cd build
    cmake .. >> ../../rapmap_install.log 2>&1
    retcmake=$?
    make >> ../../rapmap_install.log 2>&1
    retmake=$?
    make install >> ../../rapmap_install.log 2>&1
    retmakeinstall=$?
    cd ../..
    if ((retcmake==0)) && ((retmake==0)) &&((retmakeinstall==0)) && [ -f rapmap/bin/rapmap ]; then
        rp=$(readlink -f rapmap/bin/)
        echo OK !!Rapmap install in $rp OK. It will be found automatically by crosscontam.
    else
        echo "!! ERROR !!"
        echo Rapmap install error
        echo
    fi
    echo
fi

# kallisto

if [[ "$TOOL" == "all" ]] || [[ "$TOOL" == "K" ]]; then
    echo "Removing old install of Kallisto (within CroCo) if it exists"
    rm -rf kallisto
    echo "Installing Kallisto"
    tar xf kallisto_linux-v0.43.0.tar.gz > kallisto_install.log 2>&1
    ret=$?
    mv kallisto_linux-v0.43.0 kallisto
    if ((ret==0)) && [ -f kallisto/kallisto ]; then
        bp=$(readlink -f kallisto/)
        echo OK !! Kallisto install in $bp OK. It will be found automatically by crosscontam.
    else
        echo "!! ERROR !!"
        echo  install error
        echo
    fi
    echo
fi

## salmon
#if [[ "$TOOL" == "all" ]] || [[ "$TOOL" == "S" ]]; then
#    echo "Removing old install of Salmon (within CroCo) if it exists"
#    rm -rf salmon
#    # MACOS
#    if [[ "$OS" == "macosx" ]]; then
#        tar xvf SalmonBeta-0.6.0_OSX_10.11.tar.gz > salmon_install.log 2>&1
#        ret=$?
#        mv SalmonBeta-0.6.0_OSX_10.11/ salmon
#        mkdir salmon/install
#        mv salmon/bin salmon/lib salmon/install
#        if ((ret==0)) && [ -f salmon/install/bin/salmon ]; then
#            sp=$(readlink -f salmon/install/bin/)
#            echo OK Salmon install in $sp OK. It will be found automatically by crosscontam.
#        else
#            echo "!! ERROR !!"
#            echo Salmon install error
#            echo
#        fi
#    # LINUX
#    else
#        echo "Installing Salmon. This may take more than 5 minutes."
#        tar xf salmon.tar.gz > salmon_install.log 2>&1
#        cd salmon
#        mkdir build install
#        cd build
#        cmake -DFETCH_BOOST=TRUE -DCMAKE_INSTALL_PREFIX=../install ../ >> ../../salmon_install.log 2>&1
#        retcmake=$?
#        make >> ../../salmon_install.log 2>&1
#        retmake=$?
#        make install >> ../../salmon_install.log 2>&1
#        retmakeinstall=$?
#        cd ../..
#
#        if ((retcmake==0)) && ((retmake==0)) && ((retmakeinstall==0)) && [ -f salmon/install/bin/salmon ]; then
#            sp=$(readlink -f salmon/install/bin/)
#            echo OK Salmon install in $sp OK. It will be found automatically by crosscontam.
#        else
#            echo "!! ERROR !!"
#            echo Salmon install error
#            echo
#        fi
#    fi
#    echo
#fi

# NCBI Blast

if [[ "$TOOL" == "all" ]] || [[ "$TOOL" == "BL" ]]; then
    echo "Removing old install of ncbi-blast if it exists"
    rm -rf ncbi-blast-2.5.0+
    echo "Installing ncbi blast LATEST version. This may take a few minutes."
    if [[ "$OS" == "macosx" ]]; then
        tar xzvf ncbi-blast-2.5.0+-x64-macosx.tar.gz > ncbi-blast_install.log 2>&1
    else
        tar xzvf ncbi-blast-2.5.0+-x64-linux.tar.gz > ncbi-blast_install.log 2>&1
    fi

    if  [ -f ncbi-blast-*/bin/blastn ]; then
        blast=$(readlink -f ncbi-blast-*/bin/)
        echo OK ncbi-blast install in $blast is OK. It will be found automatically by crosscontam.
    else
        echo "!! ERROR !!"
        echo ncbi-blast install error
        echo
    fi
    echo
fi


# hpg-aligner

#if [[ "$TOOL" == "all" ]] || [[ "$TOOL" == "H" ]]; then
#    echo "Removing old install of hpg-aligner if it exists"
#    rm -rf hpg-aligner
#    if [[ "$OS" == "macosx" ]]; then
#        which scons > /dev/null
#        retscons=$?
#        if ((retscons == 0)); then
#            echo "scons was found on your system."
#        else
#            echo "You must install scons to compile hpg-aligner."
#            exit 0
#        fi
#    fi
#    echo "Installing hpg-aligner. This may take a few minutes."
#    tar xf hpg-aligner.tar.gz > hpg-aligner_install.log 2>&1
#    cd hpg-aligner
#    scons >> ../hpg-aligner_install.log 2>&1
#    ret=$?
#    cd ..
#    if ((ret==0)) && [ -f hpg-aligner/bin/hpg-aligner ]; then
#        hp=$(readlink -f hpg-aligner/bin/)
#        echo OK hpg-aligner install in $hp OK. It will be found automatically by crosscontam.
#    else
#        echo "!! ERROR !!"
#        echo hpg-aligner install error
#        echo
#    fi
#    echo
#fi
