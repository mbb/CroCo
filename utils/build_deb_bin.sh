#!/bin/bash

function printUsage(){
echo "usage : 
$0  (-p | --path) path_to_files (-v | --version)  version number (-n | --name) program name

You have to give all the parameters

"
}

ARGS=$(getopt -o v:n: -l "version:name:" -n "$0" -- "$@");

#Bad arguments
if [ $? -ne 0 ] || [ $# -eq 0 ];
then
      printUsage
    exit
fi
NAMEFLAG=0
VERSIONFLAG=0
eval set -- "$ARGS";

while true; do
  case "$1" in
    -n|--name)
      shift;
      if [ -n "$1" ]; then
          NAME=$1
          NAMEFLAG=1
      fi
      shift;
      ;;
    -v|--version)
      shift;
      if [ -n "$1" ]; then
          VERSION=$1
          VERSIONFLAG=1
      fi
      shift;
      ;;
    --)
      shift;
      break;
      ;;
  esac
done

if [ $VERSIONFLAG == 0 ] || [ $NAMEFLAG == 0 ]; then
    printUsage
    exit
fi

ARCH="all"
PACKAGEDIR=$NAME\_$VERSION\_$ARCH
rm -rf $PACKAGEDIR

# template copy 
echo $PACKAGEDIR
cp -rp pkg-template/ $PACKAGEDIR/

mkdir -p $PACKAGEDIR/usr/bin/
cp -rp ../src/crossconta-detection $PACKAGEDIR/usr/bin/$NAME
cp -rp ../src/crossconta-detection.pl $PACKAGEDIR/usr/bin/${NAME}.pl
cp -rp ../src/parsebowtie.pl $PACKAGEDIR/usr/bin/
mv $PACKAGEDIR/usr/share/doc/crossconta-detection $PACKAGEDIR/usr/share/doc/$NAME
mv $PACKAGEDIR/usr/share/man/man1/crossconta-detection.1 $PACKAGEDIR/usr/share/man/man1/${NAME}.1
chmod -R 755 $PACKAGEDIR/usr/bin/  $PACKAGEDIR/usr  $PACKAGEDIR/usr/share  $PACKAGEDIR/usr/share/doc  $PACKAGEDIR/usr/share/doc/$NAME

# package generation

# gestion du man
sed -i "s/BUILDDATE/$BUILDDATE/" $PACKAGEDIR/usr/share/man/man1/${NAME}.1
sed -i "s/crossconta-detection/$NAME/g" $PACKAGEDIR/usr/share/man/man1/${NAME}.1
sed -i "s/VersionX/Version $VERSION/" $PACKAGEDIR/usr/share/man/man1/${NAME}.1
gzip -9 $PACKAGEDIR/usr/share/man/man1/${NAME}.1
chmod 644 $PACKAGEDIR/usr/share/man/man1/${NAME}.1.gz
# control file edition
sed -i "s/Version: X/Version: $VERSION/" $PACKAGEDIR/DEBIAN/control
sed -i "s/Package: crossconta-detection/Package: $NAME/" $PACKAGEDIR/DEBIAN/control
sed -i "s/crossconta-detection/$NAME/g" $PACKAGEDIR/usr/bin/$NAME
sed -i "s/Architecture: X/Architecture: $ARCH/" $PACKAGEDIR/DEBIAN/control
git log | head -n 100 > $PACKAGEDIR/usr/share/doc/$NAME/changelog
gzip -9 $PACKAGEDIR/usr/share/doc/$NAME/changelog
chmod 644 $PACKAGEDIR/usr/share/doc/$NAME/changelog.gz

cp -r $PACKAGEDIR /tmp
cd /tmp/$PACKAGEDIR
find . -type f ! -regex '.*.git.*' ! -regex '.*?debian-binary.*' ! -regex '.*?DEBIAN.*' -printf '%P ' | xargs md5sum > DEBIAN/md5sums
cd -
chown -R root:root /tmp/$PACKAGEDIR

dpkg-deb -b /tmp/$PACKAGEDIR
mv /tmp/$PACKAGEDIR.deb ./
rm -rf /tmp/$PACKAGEDIR $PACKAGEDIR
