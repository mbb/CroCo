cd $1
### rapmap

    echo "Installing Rapmap. This may take more than 5 minutes."
    wget https://github.com/COMBINE-lab/RapMap/archive/master.zip -O RapMap-master.zip
    unzip RapMap-master.zip  2>&1
    cd RapMap-master/
    mkdir build
    cd build
    cmake .. >> ../../rapmap_install.log 2>&1
    retcmake=$?
    make >> ../../rapmap_install.log 2>&1
    retmake=$?
    make install >> ../../rapmap_install.log 2>&1
    retmakeinstall=$?
    cd ../..
    if ((retcmake==0)) && ((retmake==0)) &&((retmakeinstall==0)) && [ -f rapmap/bin/rapmap ]; then
        rp=$(readlink -f rapmap/bin/)
        echo OK !!Rapmap install in $rp OK. It will be found automatically by crosscontam.
    else
        echo "!! ERROR !!"
        echo Rapmap install error
        echo
    fi


# kallisto

    echo "Installing Kallisto"
    wget https://github.com/pachterlab/kallisto/releases/download/v0.43.0/kallisto_linux-v0.43.0.tar.gz
    tar xf kallisto_linux-v0.43.0.tar.gz > kallisto_install.log 2>&1
    ret=$?
    mv kallisto_linux-v0.43.0 kallisto
    if ((ret==0)) && [ -f kallisto/kallisto ]; then
        bp=$(readlink -f kallisto/)
        echo OK !! Kallisto install in $bp OK. It will be found automatically by crosscontam.
    else
        echo "!! ERROR !!"
        echo  install error
        echo
    fi

# salmon
        echo "Installing Salmon. This may take more than 5 minutes."
        wget https://github.com/COMBINE-lab/salmon/archive/master.zip -O salmon-master.zip
        unzip salmon-master.zip  2>&1
        cd salmon-master
        mkdir build install
        cd build
        cmake -DFETCH_BOOST=TRUE -DCMAKE_INSTALL_PREFIX=../install .. >> ../../salmon_install.log 2>&1
        retcmake=$?
        make >> ../../salmon_install.log 2>&1
        retmake=$?
        make install >> ../../salmon_install.log 2>&1
        retmakeinstall=$?
        cd ../..

        if ((retcmake==0)) && ((retmake==0)) && ((retmakeinstall==0)) && [ -f salmon-master/install/bin/salmon ]; then
            sp=$(readlink -f salmon-master/install/bin/)
            echo OK Salmon install in $sp OK. It will be found automatically by crosscontam.
        else
            echo "!! ERROR !!"
            echo Salmon install error
            echo
        fi
