# summary of settings
if [ $RECAT != "no" ]; then
 echo "RE-CATEGORIZATION"
 echo
 echo "previous output directory to re-use : $RECAT"
 echo
 echo "SETTINGS"
 echo
 echo "in : $INDIR"
 echo "fold-threshold : $FOLD"
 echo "minimum-coverage : $MINCOV"
 echo "output-prefix : $OUTPUTPREFIX"
 echo "output-level : $OUTPUTLEVEL"
 echo "graph : $GRAPH"
 echo "overexpression : $OVEREXP"
 echo
elif  [ $RECAT == "no" ]; then
 echo "FULL CROCO RUN"
 echo
 echo "SETTINGS"
 echo
 echo "mode : $MODE"
 echo "in : $INDIR"
 echo "tool : $TOOL"
 echo "fold-threshold : $FOLD"
 echo "trim5 : $TRIM5"
 echo "trim3 : $TRIM3"
 echo "minimum-coverage : $MINCOV"
 echo "threads : $PROCESSORS"
 echo "output-prefix : $OUTPUTPREFIX"
 echo "output-level : $OUTPUTLEVEL"
 echo "graph : $GRAPH"
 echo "add-option : $ADDOPT"
 echo "frag-length : $FRAGLENGTH"
 echo "frag-sd : $FRAGSD"
 echo "suspect_id : $SUSPID"
 echo "suspect_len : $SUSPLEN"
 echo "overexpression : $OVEREXP"
 echo
fi
