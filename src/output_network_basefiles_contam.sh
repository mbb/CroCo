### OUTPUT : network files ("LINKS_exhaustive.csv" and "nodes_detailed.csv")
cd $out
echo -e "\nCreating network output files"
echo -e "\tLINKS files\n\tNODES files"
#echo -e "\nCreating network output files\n\tLINKS_exhaustive.csv\n\tnodes_detailed.csv"
echo -e "Source,Target,Type,Weight" > LINKS_exhaustive.csv
for sortie in *.all ; do
 #echo -e "$sortie"
 infile=`basename $sortie .all`
 awk -v INFILE=$infile 'BEGIN {currentsp=INFILE}
{ colMax= NF -2
if(NR==1) {
	for (i=2; i < NF - 2; i++) {
		sub("_reads","", $i)
		taxons[i] = $i
		tax = taxons[i]
		comptage[tax]=0
	}
}
else {
  	max = $colMax
	if ($NF == "contam") {
		for (i=2; i < NF - 2; i++) if ($i == max) {tax = taxons[i]; break}
			if (taxons[i] != currentsp) {
				print tax","currentsp",Directed,1.0" >> "LINKS_exhaustive.csv"
				comptage[tax]++
				comptage[currentsp]++
			}
	}
}
}
END{ for (i in comptage) {
		print i"\tnb_contaminants = "comptage[i]"\tsp_contaminated = "currentsp >> "nodes_detailed.csv"
	}
	print "total contaminations "currentsp" = "comptage[currentsp]"\n" >> "nodes_detailed.csv"
}
 ' $sortie 2>/dev/null
done
