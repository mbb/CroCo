# add sample name to all sequence names and build BLASTdb
for (( j=0; j <i; j++ )); do
	ref=${fasta_array[$j]}
	awk '/^>/{ print $1 } ; /^[^>]/{ print $0 }' < $INDIR/${fasta_array[$j]}.fasta > $out/${fasta_array[$j]}".fasta_mod"
	sed -i "s/>/>$ref|/g" $out/${fasta_array[$j]}".fasta_mod"
	makeblastdb -in $out/${fasta_array[$j]}".fasta_mod" -parse_seqids -dbtype nucl -out $out/${fasta_array[$j]}".blastdb"
done


### initial version of the section :
# "all pairwise BLAST and listing suspects (see $SUSPID and $SUSPLEN)"
 echo -e "\n"
 for (( j=0; j <i; j++ )); do
    ref=${fasta_array[$j]};
  suspects=$out/$ref".suspects"
  for (( k=0; k <i; k++ )); do
   target=${fasta_array[$k]};
   if [ "$ref" != "$target" ]; then
    outblast=$out/$ref"v"$target".outblast"
    query=$out/$ref".fasta_mod"
    db=$out/$target".blastdb"
    echo -e "Blasting $ref vs. $target"
    blastn -num_threads $PROCESSORS -query $query -db $db -perc_identity $SUSPID -soft_masking true -max_target_seqs 5000 -outfmt "6 qseqid sseqid evalue pident bitscore qstart qend qlen sstart send slen" -out $outblast
   fi
  done
  cat $out/$ref"v"*".outblast" > $out/$ref".outblast" ; rm -f $out/$ref"v"*".outblast"
  cat $out/$ref".outblast" | awk -v susplen=$SUSPLEN '{ if($5>=susplen){print $1} }' > $out/$ref".suspects_tmp"
  cat $out/$ref".suspects_tmp" | sort | uniq > $out/$ref".suspects" ; rm -f $suspects"_tmp"
  blastdbcmd -db $out/$ref".blastdb" -entry_batch $out/$ref".suspects" -outfmt %f -line_length 20000 -out $out/$ref".fasta_suspect_tmp"
  cat $out/$ref".fasta_suspect_tmp" | sed 's/lcl|//g' | awk '{if($0 ~ /^>/){print $1} else{print}}' > $out/$ref".fasta_suspect"
  rm -f $out/$ref".fasta_suspect_tmp"
  echo -e "\ttotal suspects transcripts in $ref : "`cat $out/$ref".suspects" | wc -l`
 done


 # regroup all samples
 cat $out/*.fasta_mod > $out/ALL_transcripts.fasta
 cat $out/*.suspects > $out/ALL_transcripts.suspects
 cat $out/*.fasta_suspect > $out/ALL_transcripts.fasta_suspects











### TENTATIVE DE PARALLELISATION, MAIS IMPOSSIBLE DE WAIT LES PROCESSUS !!!
## all pairwise BLAST and listing suspects (see $SUSPID and $SUSPLEN)
#echo -e "\n"
#for (( j=0; j <i; j++ )); do
#	ref=${fasta_array[$j]};
#	suspects=$out/$ref".suspects"
#	for (( k=0; k <i; k++ )); do
#		target=${fasta_array[$k]};
#		if [ "$ref" != "$target" ]; then
#			echo -e "$ref\t$target" >> blast_job.list
#		fi
#	done
#done 
#
#echo -e "all-vs-all BLAST"
#split --number=l/$PROCESSORS --additional-suffix=blastjob blast_job.list
#for run in x*blastjob; do
#	(while IFS=$'\t' read ref target; do
#		outblast=$out/$ref"v"$target".outblast"
#		query=$out/$ref".fasta_mod"
#		db=$out/$target".blastdb"
#		echo -e "blastn -num_threads 1 -query $query -db $db -perc_identity $SUSPID -soft_masking true -max_target_seqs 5000 -outfmt \"6 qseqid sseqid evalue pident bitscore qstart qend qlen sstart send slen\" -out $outblast"
#
#		blastn -num_threads 1 -query $query -db $db -perc_identity $SUSPID -soft_masking true -max_target_seqs 5000 -outfmt "6 qseqid sseqid evalue pident bitscore qstart qend qlen sstart send slen" -out $outblast
#	done) & wait $!
	#pids[$run]=$!
	#echo -e "computing (processus ${pids[$run]})"
	#echo "${pids[$run]}" >> blast_job.pid
#done
#cat blast_job.pid | while read pid; do
#	echo -e "waiting for processus $pid..."
#	wait $pid
#done
#rm -f x*blastjob blast_job.list blast_job.pid

# PB : "le processus n°12862 n'est pas un fils de ce shell." !!!!!!!!!!!!

#echo -e "\nCounting suspect transcripts"
#for (( j=0; j <i; j++ )); do
#	ref=${fasta_array[$j]};
#	suspects=$out/$ref".suspects"
#	cat $out/$ref"v"*".outblast" > $out/$ref".outblast" ; rm -f $out/$ref"v"*".outblast"
#	cat $out/$ref".outblast" | awk -v susplen=$SUSPLEN '{ if($5>=susplen){print $1} }' > $out/$ref".suspects_tmp"
#	cat $out/$ref".suspects_tmp" | sort | uniq > $out/$ref".suspects" ; rm -f $suspects"_tmp"
#	blastdbcmd -db $out/$ref".blastdb" -entry_batch $out/$ref".suspects" -outfmt %f -line_length 20000 -out $out/$ref".fasta_suspect_tmp"
#	cat $out/$ref".fasta_suspect_tmp" | sed 's/lcl|//g' | awk '{if($0 ~ /^>/){print $1} else{print}}' > $out/$ref".fasta_suspect"
#	rm -f $out/$ref".fasta_suspect_tmp"
#	echo -e "\t$ref\t"`cat $out/$ref".suspects" | wc -l`
#done



