#echo -e "\nCreating html files containing detailed results"
#cp $crosscontamdir/crossContam.html .

#for output in $out/*.all; do
# ref=`basename $output .all`

#awk -v output=$output -v ref=$ref -v temps="\""$temps"\"" 'BEGIN{FS="\t"; p=0;d="\""}
#{
#    for (i=1; i<=NF; i++)  { if ($i != "inf") {a[NR,i] = $i} else {a[NR,i] ="null"}  ; if(NF>p) { p = NF }  }
#}
#END {
#    num="[[" d "1" d
#    for(j=2; j<NR; j++) {num=num ","d j d}
#    num=num"],"
#    str=""
#    for(col=1; col<=p; col++) {
#        if (col == 1 || col ==p) {d="\""} else {d=""}
#        ligne="["d""a[2,col]""d
#        for(i=3; i<=NR; i++){
#            ligne=ligne"," d""a[i,col]""d;
#        }
#        #print col, ligne
#        if (col<p)    str=str""ligne"],"
#        else str=str""ligne"]]"
#    }
#    print num str > "tmp.tmp"
#    print "/donnees/{\n  r tmp.tmp\n  d\n}"  > "insertdata.sed"
#    system("sed -f insertdata.sed crossContam.html > " output".html" )
#    nl="\\\\\\""n"
#    str="<thead>"nl"  <tr>"nl"  <th> </th>"nl
#    for(j=1; j<=p; j++) {str=str" <th>"a[1,j]"</th>"nl}
#    str= str"</tr>"nl"  </thead>"nl
#    cmd="s%entete%"str"%"
#    system( "sed -i '' -r \"" cmd "\" " output".html" )
#    system("sed -i '' \"s/espece/"ref" contigs/\" " output".html")
#    system("sed -i '' \"s/laDate/" temps "/\" " output".html")
#}' $output
#done
#rm -f tmp.tmp insertdata.sed crossContam.html "$out/*.html-r"
