############################ PATH MANAGEMENT ############################
export PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
crossScriptDir=$(readlink -f "$0")
crosscontamdir=`dirname $crossScriptDir`
crosscontamtopdir=`dirname $crosscontamdir`
bowtie_path="${crosscontamtopdir}/utils/bin/bowtie-1.1.2"
bowtie2_path="${crosscontamtopdir}/utils/bin/bowtie2-2.2.9"
kallisto_path="${crosscontamtopdir}/utils/bin/kallisto/"
Salmon_path="${crosscontamtopdir}/utils/bin/salmon/install/bin"
RapMap_path="${crosscontamtopdir}/utils/bin/rapmap/bin/"
HPG_path="${crosscontamtopdir}/utils/bin/hpg-aligner/bin/"
BLAST_path="${crosscontamtopdir}/utils/bin/ncbi-blast-2.5.0+/bin"
export PATH=$bowtie_path:$bowtie2_path:$kallisto_path:$Salmon_path:$RapMap_path:$HPG_path:$BLAST_path:$PATH
